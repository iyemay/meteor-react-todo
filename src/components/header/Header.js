import React, {Component} from "react";

export class Header extends Component {
    render() {
        return (
            <div className="d-flex justify-content-center mt-5">
                <h1 className="mt-3 header-text">Meteor ReactTodo</h1>
            </div>
        );
    }
}